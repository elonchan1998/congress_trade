
# %%

import pandas as pd
from os import listdir


import statsmodels.api as sm
import statsmodels.formula.api as smf
# %%

name_cid = pd.read_csv('cid_log.csv')
name_cid = name_cid[name_cid['name'] != 'Ted Kaufman (D-Del)'] #this man has wired record


name_cid.drop_duplicates(inplace=True, ignore_index=True)

name_cid['name'] = name_cid['name'].str.split(' ').str[:-1]

names = []
for name in name_cid['name']:
    names.append(' '.join([word for word in name if "." not in word]))

name_cid['name'] = names
# %%

#load stock list
amex = pd.read_csv('./Data/stocks/stock_list/AMEX.txt', sep="\t")
nasdaq = pd.read_csv('./Data/stocks/stock_list/NASDAQ.txt', sep="\t")
nyse = pd.read_csv('./Data/stocks/stock_list/NYSE.txt', sep="\t")

stock_list = pd.concat([amex, nasdaq, nyse])
stock_list.sort_values(by='Symbol', inplace=True)
stock_list.set_index('Symbol', inplace=True)

unique_stock_code = pd.read_csv('./Data/stocks/stock_list/unique_stock_code.csv', index_col=0)
# %%

# load factors
three_factors = pd.read_csv('./Data/regression/factors/F-F_Research_Data_Factors_daily.CSV', skiprows=4, skipfooter=3)
three_factors.rename(columns={'Unnamed: 0': 'Date'}, inplace=True)
three_factors['Date'] = pd.to_datetime(three_factors['Date'], format="%Y%m%d")

five_factors = pd.read_csv('./Data/regression/factors/F-F_Research_Data_5_Factors_2x3_daily.CSV', skiprows=3)
five_factors.rename(columns={'Unnamed: 0': 'Date'}, inplace=True)
five_factors['Date'] = pd.to_datetime(five_factors['Date'], format='%Y%m%d')


# %%

name_detail = pd.read_csv('./Data/names/congressmen_details_after08.csv', index_col=0)



# %%

# Congressmen from different parties, 1 month holding period

df_reprent_1m = pd.DataFrame()
df_senate_1m = pd.DataFrame()
i = 0

for file in sorted(listdir('./Data/clean_individual_record')):

    i += 1
    #if i < 14:
        #pass
        #continue
    
    cid = file[:-4]
    name = name_cid[name_cid['cid'] == cid]['name'].iloc[0]
    trading_record = pd.read_csv(f'./Data/clean_individual_record/{file}', index_col=0)
    
    # find the congressmen's party
    house = name_detail[name_detail['Congressman'] == name]['House']
    if house.empty:
        continue
    else:
        house = house.values[0]


    # purchase only
    trading_record = trading_record[trading_record['action'] == 'Purchased']

    # drop all before 2008
    #trading_record = trading_record.loc[pd.to_datetime(trading_record['date1']) > pd.Timestamp(2008,1,1)]

    if trading_record.empty:
        continue

    for _, (stock, purchase_date) in trading_record[['orgname', 'date1']].iterrows():

        matched_code = stock_list[stock_list['Description'] == stock]

        if matched_code.empty:
            continue

        stock_code = matched_code.index[0]
        stock_data = pd.read_csv(f'./Data/stocks/{stock_code}.csv')

        if purchase_date not in stock_data['Date'].values:
            continue

        starting_point = stock_data[stock_data['Date'] == purchase_date].index[0]
        one_month = stock_data.loc[starting_point: starting_point+20]

        one_month_return = one_month[stock_code]/one_month[stock_code].shift(1) - 1
        one_month_return.dropna(inplace=True)
        
        temp = pd.DataFrame(data={'Date': one_month.loc[one_month_return.index]['Date'], 'Return': one_month_return})

        if house == 'Senate':
            df_senate_1m = pd.concat([df_senate_1m, temp])
        elif house == 'Representative':
            df_reprent_1m = pd.concat([df_reprent_1m, temp])
        else:
            # consider republic and democrat only
            continue

    if i%10 == 0:
        print(i)
# %%
df_senate_1m = pd.read_csv('./Data/regression/senate_1m.csv', index_col=0)
df_senate_1m = df_senate_1m.sort_values(by=['Date'])

df_reprent_1m = pd.read_csv('./Data/regression/represent_1m.csv', index_col=0)
df_reprent_1m = df_reprent_1m.sort_values(by=['Date'])
# %%

df_senate_1m['Date'] = pd.to_datetime(df_senate_1m['Date'])
df_senate_1m_3f = df_senate_1m.merge(three_factors, left_on='Date', right_on='Date')
df_senate_1m_5f = df_senate_1m.merge(five_factors, left_on='Date', right_on='Date')

df_reprent_1m['Date'] = pd.to_datetime(df_reprent_1m['Date'])
df_reprent_1m_3f = df_reprent_1m.merge(three_factors, left_on='Date', right_on='Date')
df_reprent_1m_5f = df_reprent_1m.merge(five_factors, left_on='Date', right_on='Date')
# %%

Y = df_senate_1m_3f['Return']
X = df_senate_1m_3f['Mkt-RF']
res = sm.OLS(Y, sm.add_constant(X)).fit()
print(res.summary())
# %%

Y = df_reprent_1m_3f['Return']
X = df_reprent_1m_3f['Mkt-RF']
res = sm.OLS(Y, sm.add_constant(X)).fit()
print(res.summary())
# %%

Y = df_senate_1m_3f['Return']
X = df_senate_1m_3f[['Mkt-RF', 'SMB', 'HML']]
res = sm.OLS(Y, sm.add_constant(X)).fit()
print(res.summary())
# %%

Y = df_reprent_1m_3f['Return']
X = df_reprent_1m_3f[['Mkt-RF', 'SMB', 'HML']]
res = sm.OLS(Y, sm.add_constant(X)).fit()
print(res.summary())
# %%

Y = df_senate_1m_5f['Return']
X = df_senate_1m_5f[['Mkt-RF', 'SMB', 'HML', 'RMW', 'CMA']]
res = sm.OLS(Y, sm.add_constant(X)).fit()
print(res.summary())
# %%

Y = df_reprent_1m_5f['Return']
X = df_reprent_1m_5f[['Mkt-RF', 'SMB', 'HML', 'RMW', 'CMA']]
res = sm.OLS(Y, sm.add_constant(X)).fit()
print(res.summary())
# %%

# year 2008-2012
df_senate_1m_3f_before = df_senate_1m_3f[df_senate_1m_3f['Date'] < pd.Timestamp(2012, 4, 4)]
df_senate_1m_5f_before = df_senate_1m_5f[df_senate_1m_5f['Date'] < pd.Timestamp(2012, 4, 4)]

df_reprent_1m_3f_before = df_reprent_1m_3f[df_reprent_1m_3f['Date'] < pd.Timestamp(2012, 4, 4)]
df_reprent_1m_5f_before = df_reprent_1m_5f[df_reprent_1m_5f['Date'] < pd.Timestamp(2012, 4, 4)]

# after the stock act
df_senate_1m_3f_after = df_senate_1m_3f[df_senate_1m_3f['Date'] >= pd.Timestamp(2012, 4, 4)]
df_senate_1m_5f_after = df_senate_1m_5f[df_senate_1m_5f['Date'] >= pd.Timestamp(2012, 4, 4)]

df_reprent_1m_3f_after = df_reprent_1m_3f[df_reprent_1m_3f['Date'] >= pd.Timestamp(2012, 4, 4)]
df_reprent_1m_5f_after = df_reprent_1m_5f[df_reprent_1m_5f['Date'] >= pd.Timestamp(2012, 4, 4)]
# %%

Y = df_senate_1m_3f_before['Return']
X = df_senate_1m_3f_before['Mkt-RF']

res = sm.OLS(Y, sm.add_constant(X)).fit()
print(res.summary())
# %%

Y = df_reprent_1m_3f_before['Return']
X = df_reprent_1m_3f_before['Mkt-RF']

res = sm.OLS(Y, sm.add_constant(X)).fit()
print(res.summary())

# %%


Y = df_senate_1m_3f_after['Return']
X = df_senate_1m_3f_after['Mkt-RF']

res = sm.OLS(Y, sm.add_constant(X)).fit()
print(res.summary())
# %%

Y = df_reprent_1m_3f_after['Return']
X = df_reprent_1m_3f_after['Mkt-RF']

res = sm.OLS(Y, sm.add_constant(X)).fit()
print(res.summary())
# %%

Y = df_senate_1m_3f_before['Return']
X = df_senate_1m_3f_before[['Mkt-RF', 'SMB', 'HML']]
res = sm.OLS(Y, sm.add_constant(X)).fit()
print(res.summary())
# %%

Y = df_reprent_1m_3f_before['Return']
X = df_reprent_1m_3f_before[['Mkt-RF', 'SMB', 'HML']]
res = sm.OLS(Y, sm.add_constant(X)).fit()
print(res.summary())

# %%

Y = df_senate_1m_3f_after['Return']
X = df_senate_1m_3f_after[['Mkt-RF', 'SMB', 'HML']]
res = sm.OLS(Y, sm.add_constant(X)).fit()
print(res.summary())
# %%

Y = df_reprent_1m_3f_after['Return']
X = df_reprent_1m_3f_after[['Mkt-RF', 'SMB', 'HML']]
res = sm.OLS(Y, sm.add_constant(X)).fit()
print(res.summary())
# %%

Y = df_senate_1m_5f_before['Return']
X = df_senate_1m_5f_before[['Mkt-RF', 'SMB', 'HML', 'RMW', 'CMA']]
res = sm.OLS(Y, sm.add_constant(X)).fit()
print(res.summary())
# %%

Y = df_reprent_1m_5f_before['Return']
X = df_reprent_1m_5f_before[['Mkt-RF', 'SMB', 'HML', 'RMW', 'CMA']]
res = sm.OLS(Y, sm.add_constant(X)).fit()
print(res.summary())
# %%

Y = df_senate_1m_5f_after['Return']
X = df_senate_1m_5f_after[['Mkt-RF', 'SMB', 'HML', 'RMW', 'CMA']]
res = sm.OLS(Y, sm.add_constant(X)).fit()
print(res.summary())
# %%

Y = df_reprent_1m_5f_after['Return']
X = df_reprent_1m_5f_after[['Mkt-RF', 'SMB', 'HML', 'RMW', 'CMA']]
res = sm.OLS(Y, sm.add_constant(X)).fit()
print(res.summary())
# %%
