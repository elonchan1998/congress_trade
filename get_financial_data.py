# %%
import pandas as pd
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
import re

# %%
congressmen_names = pd.read_csv('./Data/names/congressmen_names_after08.csv')
congressmen_names.drop(['Unnamed: 0'], axis=1, inplace=True)

last_names = congressmen_names.iloc[:, 0].str.split().str[-1]
given_names = congressmen_names.iloc[:, 0].str.split().str[:-1].str.join('-')
first_names = congressmen_names.iloc[:, 0].str.split().str[0]

# %%

chrome_options = Options()
chrome_options.add_extension(r'/home/elon/python/Insider/extension_2_0_6_0.crx')
chrome_options.add_extension(r'/home/elon/python/Insider/extension_0_0_3_0.crx')
prefs = {'download.default_directory' : '/home/elon/python/Insider/Data/'}
chrome_options.add_experimental_option('prefs', prefs)

driver = webdriver.Chrome(options=chrome_options)
driver.get('https://www.opensecrets.org/personal-finances')

# %%
'''
driver.get(r"https://stackoverflow.com/users/signup?ssrc=head&returnurl=%2fusers%2fstory%2fcurrent")
time.sleep(5)
driver.find_element_by_css_selector('button.grid--cell.s-btn.s-btn__icon.s-btn__google.bar-md.ba.bc-black-3').click()
time.sleep(6)
driver.find_element_by_id('identifierId').send_keys('elonchan1998')
time.sleep(1)
driver.find_element_by_id('identifierNext').click()
time.sleep(5)
driver.find_element_by_name("password").send_keys('Tortoise#123')
driver.find_element_by_id("passwordNext").click()
time.sleep(5)

driver.find_element_by_tag_name('body').send_keys(Keys.COMMAND + 'w')
time.sleep(2)
'''

log = pd.read_csv('log.csv')
cid_log = pd.read_csv('cid_log.csv')

for i, last_name in enumerate(last_names.unique()[815:]):  # should be 915 unique last names
    time.sleep(1)
    driver.get('https://www.opensecrets.org/personal-finances/search?q={}&type=person'.format(last_name))
    time.sleep(2)
    table = driver.find_element_by_id('DataTables_Table_0')

    # df = pd.read_html(table.get_attribute('outerHTML'))[0]

    columns_1 = table.find_elements_by_xpath('//tr/td[1]')
    columns_2 = table.find_elements_by_xpath('//tr/td[2]')
    

    # To avoid stale element reference exception
    congressmen = []
    congressmen_first = []
    list_of_list_of_years = []
    list_of_links_text = []

    for result_name, result_years in zip(columns_1, columns_2):
        if result_years.get_attribute('innerText').split('\n')[0] in ['N/A', ''] :
            print(result_name.get_attribute('innerText'), ' link is N/A')
            continue
        
        congressmen.append(result_name.get_attribute('innerText'))

        # to filter name started with sth like J. Randy Forbes
        temp_name = congressmen[-1].split()
        if '.' in temp_name[0]:
            congressmen_first.append(congressmen[-1].split()[1])
        else:
            congressmen_first.append(congressmen[-1].split()[0])
            
        list_of_list_of_years.append(result_years.get_attribute('innerText').split('\n'))
        list_of_links_text.append(result_years.find_elements_by_tag_name('a')[0].get_attribute('outerHTML'))


    for j in range(len(list_of_list_of_years)):

        congressman_first = congressmen_first[j]

        # check if current name is true last name or merely containing it
        if ' ' + last_name not in congressmen[j]:
            print(' '+last_name, ' not equal: ', congressmen[j])
            continue

        list_of_years = list_of_list_of_years[j]

        links_text = list_of_links_text[j]
        cid = re.findall('=([A-Za-z][0-9]+)&', links_text)[0]

        ############### log the cid
        cid_log = cid_log.append({'name': congressmen[j], 'cid': cid}, ignore_index=True)
        ###############


        for year in list_of_years:

            if congressman_first + ' ' + last_name + ' ' + str(year) in list(log['records']):
                print('Record for {} in year {} already existed'.format(congressmen[j], year))
                continue
            
            driver.get('https://www.opensecrets.org/personal-finances/transactions/{}-{}?cid={}&year={}'.format(congressman_first, last_name, cid, year))
            time.sleep(2)

            if 'No transactions were found' in driver.page_source:
                print(congressmen[j],'No record in year {}'.format(year))
                log = log.append({'records': str(congressman_first) + ' ' + str(last_name) + ' ' + str(year)}, ignore_index=True)
                continue

            else:
                time.sleep(2)
                driver.get('https://www.opensecrets.org/personal-finances/transactions/{}-{}.csv?cid={}&year={}'.format(congressman_first, last_name, cid, year))

                log = log.append({'records': str(congressman_first) + ' ' + str(last_name) + ' ' + str(year)}, ignore_index=True)


    if i%5 == 0:
        log.to_csv('log.csv', index=False)
        cid_log.to_csv('cid_log.csv', index=False)
        print(i, 'Saved')


    '''
    for column in columns:

        # years
        list_of_years = column.text.split('\n')

        # find cid
        links_text = column.find_elements_by_tag_name('a')[0].get_attribute('outerHTML')
        cid = re.findall('=([A-Za-z][0-9]+)&', links_text)[0]

        # open a new tab to avoid stale element reference
        driver.find_element_by_tag_name('body').send_keys(Keys.COMMAND + 't')
        for year in list_of_years:
            driver.get('https://www.opensecrets.org/personal-finances/transactions/{}-{}?cid={}&year={}'.format(first_names[i], last_name, cid, year))
            time.sleep(1)
            print('https://www.opensecrets.org/personal-finances/transactions/{}-{}?cid={}&year={}'.format(first_names[i], last_name, cid, year))
        
        # close the tab to read column again
        driver.find_element_by_tag_name('body').send_keys(Keys.COMMAND + 'w') 
    '''
    
    '''
    for j, row in enumerate(df):
        list_of_years = df.Profiles[j].split('  ')

        # find cid
        elems = table.find_elements_by_xpath("//a[@href]")
        for elem in elems:
            href = elem.get_attribute('href')
            cids = []
            if href is not None and 'cid' in href:
                cids.append(re.findall('=([A-Za-z][0-9]+)&', href)[0])


        for year in list_of_years:
            driver.get('https://www.opensecrets.org/personal-finances/transactions/{}-{}?cid={}&year={}'.format(first_names[i], last_name, cid, year))
            time.sleep(1)
    '''


# %%


break at save 20, restart from last_names.unique()[20:]
break at save 95, restart from last_names.unique()[115:]
break at save 115, restart from last_names.unique()[230:]
break at save 30, restart from last_names.unique()[260:]
break at save 175, restart from last_names.unique()[435:]
break at save 380, restart from last_names.unique()[815:]
finished at 99, total total 915