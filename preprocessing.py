# %%
import pandas as pd
import os, fnmatch

import re
import glob

# %%
name_cid = pd.read_csv('cid_log.csv')
name_cid = name_cid[name_cid['name'] != 'Ted Kaufman (D-Del)'] #this man has wired record
name_cid.drop_duplicates(inplace=True, ignore_index=True)

name_cid['name'] = name_cid['name'].str.split(' ').str[:-1]

names = []
for name in name_cid['name']:
    names.append(' '.join([word for word in name if "." not in word]))

name_cid['name'] = names

# %%

# Read Congressmen data
current_representatives = pd.read_excel('./Data/names/current_representatives.xlsx', index_col=0)
current_representatives.drop(['Party'], axis=1, inplace=True)
current_representatives = current_representatives.rename(columns={"Party.1": "Party"})

current_senators = pd.read_excel('./Data/names/current_senators.xlsx', index_col=0)
current_senators.drop(['Party'], axis=1, inplace=True)
current_senators = current_senators.rename(columns={"Party.1": "Party"})

previous_representatives = pd.read_excel('./Data/names/previous_representatives.xlsx', index_col=0)

previous_senators = pd.read_excel('./Data/names/previous_senators.xlsx', index_col=0)

# Exclude record before 2008
previous_representatives = previous_representatives[previous_representatives.Years.str[-4:].astype(int) >= 2008]
previous_senators = previous_senators[previous_senators.Years.str[-4:].astype(int) >= 2008]


# %%

# Unift the congressmen data
start_year = previous_senators['Years'].str[:4]
end_year = previous_senators['Years'].str[-4:]

previous_senators['Incumbency'] = end_year.astype(int)-start_year.astype(int)
previous_senators['House'] = 'Senate'
previous_senators.drop(['Class', 'LifeSpan', 'Years'], axis=1, inplace=True)


start_year = previous_representatives['Years'].str[:4]
end_year = previous_representatives['Years'].str[-4:]

previous_representatives['Incumbency'] = end_year.astype(int) - start_year.astype(int)
previous_representatives['House'] = 'Representative'
previous_representatives.drop(['Years', 'LifeSpan'], axis=1, inplace=True)

previous_representatives.rename(columns={'Representative': 'Congressman'}, inplace=True)
previous_senators.rename(columns={'Senator': 'Congressman'}, inplace=True)

previous_congressmen = pd.concat([previous_representatives, previous_senators], ignore_index=True)


current_representatives['District'] = current_representatives['District'].str.replace('\d+', '')

current_representatives.dropna(inplace=True) # Drop vacancy
current_representatives['Incumbency'] = 2020 - current_representatives['Assumed office'].str.extract('(\d+)').astype(int)
current_representatives['House'] = 'Representative'
current_representatives.drop(['Prior experience', 'Education', 'Assumed office', 'Residence', 'Born'], axis=1, inplace=True)

current_senators['Incumbency'] = 2020 - current_senators['Assumed office'].str.extract(r'(\d{4})').astype(int)
current_senators['House'] = 'Senate'
current_senators.drop(['Image', 'Born', 'Occupation(s)', 'Previousoffice(s)', 'Assumed office', 'Term up', 'Residence'], axis=1, inplace=True)

current_representatives.rename(columns={'District': 'State', 'Member': 'Congressman'}, inplace=True)
current_senators.rename(columns={'Senator': 'Congressman'}, inplace=True)

current_congressmen = pd.concat([current_representatives, current_senators], ignore_index=True)


current_congressmen = current_congressmen[['Congressman', 'State', 'Party', 'Incumbency', 'House']]

all_congressmen = pd.concat([previous_congressmen, current_congressmen], ignore_index=True)

# %%
for index, row in name_cid.iterrows():
    name = row['name']
    cid = row['cid']
    matched_files = glob.glob(f'./Data/transactions/*{cid}*.csv')

    # Pass if the congressman has no transaction record
    if matched_files == []:
        continue

    df_individual = pd.DataFrame()
    for file in matched_files:
        try:
            # N99999863, N99999858 and N99999887 are empty files
            temp_df = pd.read_csv(file)
        except Exception as e:
            print(name, cid, e)
            continue
        df_individual = pd.concat([df_individual, temp_df])
        try:
            # Some files in N00031227, N00029675, N00031545 and N00026710 are without date
            df_individual['date1'] = pd.to_datetime(df_individual['date1'], errors='coerce')
        except Exception as e:
            print(name, cid, e)
            continue
        df_individual.sort_values(by=['date1', 'orgname'], inplace=True, ascending=True)
        df_individual.to_csv(f'./Data/individual_record/{cid}.csv')
        

# %%

# %%
