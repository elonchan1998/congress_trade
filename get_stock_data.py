# %%

# This file is to download stock data of congressmen's trade

import pandas as pd
import glob
from os import listdir
import numpy as np
import yfinance as yf
from pandas_datareader import data


# %%
name_cid = pd.read_csv('cid_log.csv')
name_cid = name_cid[name_cid['name'] != 'Ted Kaufman (D-Del)'] #this man has wired record


name_cid.drop_duplicates(inplace=True, ignore_index=True)

name_cid['name'] = name_cid['name'].str.split(' ').str[:-1]

names = []
for name in name_cid['name']:
    names.append(' '.join([word for word in name if "." not in word]))

name_cid['name'] = names

# %%
# get the full stock list on the three major stock exchanges
amex = pd.read_csv('./Data/stocks/stock_list/AMEX.txt', sep="\t")
nasdaq = pd.read_csv('./Data/stocks/stock_list/NASDAQ.txt', sep="\t")
nyse = pd.read_csv('./Data/stocks/stock_list/NYSE.txt', sep="\t")

stock_list = pd.concat([amex, nasdaq, nyse])
stock_list.sort_values(by='Symbol', inplace=True)
stock_list.set_index('Symbol', inplace=True)
# %%

# Clean the file, run once only

for file in sorted(listdir('./Data/individual_record')):
    matched_file = pd.read_csv(f'./Data/individual_record/{file}')

    # drop a unnamed
    matched_file.drop(['Unnamed: 0','ultorg', 'transnotes'], axis=1, inplace=True)

    # remove records without date
    matched_file.dropna(subset=['date1'], inplace=True)

    matched_file.to_csv(f'./Data/clean_individual_record/{file}')

# %%
i = 0
stock_code_list = []
for file in sorted(listdir('./Data/clean_individual_record')):
    matched_file = pd.read_csv(f'./Data/clean_individual_record/{file}', index_col=0)

    purchase_only = matched_file[matched_file['action'] == 'Purchased']

    if purchase_only.empty:
        print(file, 'is empty')
        continue

    for stock in purchase_only['orgname']:
        stock_code = stock_list[stock_list['Description'] == stock]
        
        if not stock_code.empty:
            stock_code_list.append(stock_code.index.values[0])

    
# %%
unique_stock_code = sorted(set(stock_code_list))
unique_stock_code = pd.DataFrame(unique_stock_code)
unique_stock_code.to_csv('./Data/stocks/stock_list/unique_stock_code.csv')
# %%

unique_stock_code = pd.read_csv('./Data/stocks/stock_list/unique_stock_code.csv', index_col=0)

for stock in unique_stock_code.values:
    try:
        hist = data.DataReader(stock[0], 
                        start='2008-1-1', 
                        end='2019-12-31', 
                        data_source='yahoo')
        hist.rename(columns={"Adj Close": stock[0]}, inplace=True)
        hist[stock[0]].to_csv(f'./Data/stocks/{stock[0]}.csv')

    except Exception as e:
        print(e)

        # use yfinance to grab all
        ticker = yf.Ticker(stock[0])
        hist = ticker.history(period='MAX')
        hist.rename(columns={'Close': stock[0]}, inplace=True)
        hist[stock[0]].to_csv(f'./Data/stocks/{stock[0]}.csv')

    # %%
