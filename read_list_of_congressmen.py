# %%
from selenium import webdriver 
import pandas as pd
import re
import string
import time


# %%

# All Previous Senators
driver = webdriver.Chrome()
driver.get('https://en.wikipedia.org/wiki/List_of_former_United_States_senators')

tables = driver.find_elements_by_css_selector('table.wikitable.sortable.jquery-tablesorter')
tables = tables[1:]  # The first table containing years only

previous_senator = pd.DataFrame(columns=['Senator', 'Years', 'Class', 'State', 'Party', 'LifeSpan'])
for table in tables:
    df = pd.read_html(table.get_attribute('outerHTML'))[0]
    df.columns = previous_senator.columns
    previous_senator = previous_senator.append(df)
    
driver.close()

previous_senator.reset_index(drop=True, inplace=True)
# %%

# All previous representatives
driver = webdriver.Chrome()
previous_representatives = pd.DataFrame(columns=['Representative', 'Years', 'State', 'Party', 'LifeSpan'])
for alphabet in string.ascii_uppercase.replace('X', ''): 
    driver.get('https://en.wikipedia.org/wiki/List_of_former_members_of_the_United_States_House_of_Representatives_({})'.format(alphabet))
    time.sleep(2) 
    table = driver.find_elements_by_css_selector('table.wikitable.sortable.jquery-tablesorter')[0]
    df = pd.read_html(table.get_attribute('outerHTML'))[0]
    df.columns = previous_representatives.columns
    previous_representatives = previous_representatives.append(df)

driver.close()
previous_representatives.reset_index(drop=True, inplace=True)
# %%
# All current senators
driver = webdriver.Chrome()
driver.get('https://en.wikipedia.org/wiki/List_of_current_United_States_senators')
time.sleep(1)
table = driver.find_element_by_id('senators')
current_senator = pd.read_html(table.get_attribute('outerHTML'))[0]

driver.close()

# %%
driver = webdriver.Chrome()
driver.get('https://en.wikipedia.org/wiki/List_of_current_members_of_the_United_States_House_of_Representatives')
time.sleep(1)
table = driver.find_element_by_id('votingmembers')
current_representative = pd.read_html(table.get_attribute('outerHTML'))[0]

driver.close()

# %%
temp = []

# Excluding all congressmen before 2008
temp.append(list(previous_senator[previous_senator.Years.str[-4:].astype(int) >=2008].Senator))
temp.append(list(previous_representatives[previous_representatives.Years.str[-4:].astype(int) >=2008].Representative))
temp.append(list(current_representative.Member))
temp.append(list(current_senator.Senator))

congressmen_names = [element for l in temp for element in l]
# %%

# transaction volume
# how long them hold
# different asset class